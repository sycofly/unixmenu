package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop/v6"
	"github.com/gobuffalo/validate/v3"
	"github.com/gobuffalo/validate/v3/validators"
)

// DbsAdm is used by pop to map your dbs_adms database table to your go code.
type DbsAdm struct {
	ID          int       `json:"id" db:"id"`
	Uid         int       `json:"uid" db:"uid"`
	NameF       string    `json:"name_f" db:"name_f"`
	NameS       string    `json:"name_s" db:"name_s"`
	Login       string    `json:"login" db:"login"`
	Description string    `json:"description" db:"description"`
	CreatedBy   string    `json:"created_by" db:"created_by"`
	UpdatedBy   string    `json:"updated_by" db:"updated_by"`
	DeletedBy   string    `json:"deleted_by" db:"deleted_by"`
	Deleted     bool      `json:"deleted" db:"deleted"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (d DbsAdm) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// DbsAdms is not required by pop and may be deleted
type DbsAdms []DbsAdm

// String is not required by pop and may be deleted
func (d DbsAdms) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (d *DbsAdm) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: d.ID, Name: "ID"},
		&validators.IntIsPresent{Field: d.Uid, Name: "Uid"},
		&validators.StringIsPresent{Field: d.NameF, Name: "NameF"},
		&validators.StringIsPresent{Field: d.NameS, Name: "NameS"},
		&validators.StringIsPresent{Field: d.Login, Name: "Login"},
		&validators.StringIsPresent{Field: d.Description, Name: "Description"},
		&validators.StringIsPresent{Field: d.CreatedBy, Name: "CreatedBy"},
		&validators.StringIsPresent{Field: d.UpdatedBy, Name: "UpdatedBy"},
		&validators.StringIsPresent{Field: d.DeletedBy, Name: "DeletedBy"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (d *DbsAdm) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (d *DbsAdm) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
