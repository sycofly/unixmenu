package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop/v6"
	"github.com/gobuffalo/validate/v3"
	"github.com/gobuffalo/validate/v3/validators"
)

// DbsGrp is used by pop to map your dbs_grps database table to your go code.
type DbsGrp struct {
	ID          int       `json:"id" db:"id"`
	Gid         int       `json:"gid" db:"gid"`
	Name        string    `json:"name" db:"name"`
	Description string    `json:"description" db:"description"`
	Environemnt string    `json:"environemnt" db:"environemnt"`
	Permissions string    `json:"permissions" db:"permissions"`
	CreatedBy   string    `json:"created_by" db:"created_by"`
	UpdatedBy   string    `json:"updated_by" db:"updated_by"`
	DeletedBy   string    `json:"deleted_by" db:"deleted_by"`
	Deleted     bool      `json:"deleted" db:"deleted"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (d DbsGrp) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// DbsGrps is not required by pop and may be deleted
type DbsGrps []DbsGrp

// String is not required by pop and may be deleted
func (d DbsGrps) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (d *DbsGrp) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: d.ID, Name: "ID"},
		&validators.IntIsPresent{Field: d.Gid, Name: "Gid"},
		&validators.StringIsPresent{Field: d.Name, Name: "Name"},
		&validators.StringIsPresent{Field: d.Description, Name: "Description"},
		&validators.StringIsPresent{Field: d.Environemnt, Name: "Environemnt"},
		&validators.StringIsPresent{Field: d.Permissions, Name: "Permissions"},
		&validators.StringIsPresent{Field: d.CreatedBy, Name: "CreatedBy"},
		&validators.StringIsPresent{Field: d.UpdatedBy, Name: "UpdatedBy"},
		&validators.StringIsPresent{Field: d.DeletedBy, Name: "DeletedBy"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (d *DbsGrp) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (d *DbsGrp) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
