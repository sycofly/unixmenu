package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop/v6"
	"github.com/gobuffalo/validate/v3"
	"github.com/gobuffalo/validate/v3/validators"
)

// User is used by pop to map your users database table to your go code.
type User struct {
	ID          int       `json:"id" db:"id"`
	Uid         int       `json:"uid" db:"uid"`
	NameF       string    `json:"name_f" db:"name_f"`
	NameS       string    `json:"name_s" db:"name_s"`
	Login       string    `json:"login" db:"login"`
	Description string    `json:"description" db:"description"`
	CreatedBy   string    `json:"created_by" db:"created_by"`
	UpdatedBy   string    `json:"updated_by" db:"updated_by"`
	DeletedBy   string    `json:"deleted_by" db:"deleted_by"`
	Deleted     bool      `json:"deleted" db:"deleted"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (u User) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Users is not required by pop and may be deleted
type Users []User

// String is not required by pop and may be deleted
func (u Users) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (u *User) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: u.ID, Name: "ID"},
		&validators.IntIsPresent{Field: u.Uid, Name: "Uid"},
		&validators.StringIsPresent{Field: u.NameF, Name: "NameF"},
		&validators.StringIsPresent{Field: u.NameS, Name: "NameS"},
		&validators.StringIsPresent{Field: u.Login, Name: "Login"},
		&validators.StringIsPresent{Field: u.Description, Name: "Description"},
		&validators.StringIsPresent{Field: u.CreatedBy, Name: "CreatedBy"},
		&validators.StringIsPresent{Field: u.UpdatedBy, Name: "UpdatedBy"},
		&validators.StringIsPresent{Field: u.DeletedBy, Name: "DeletedBy"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (u *User) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (u *User) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
