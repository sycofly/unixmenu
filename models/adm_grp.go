package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop/v6"
	"github.com/gobuffalo/validate/v3"
	"github.com/gobuffalo/validate/v3/validators"
)

// AdmGrp is used by pop to map your adm_grps database table to your go code.
type AdmGrp struct {
	ID          int       `json:"id" db:"id"`
	Gid         int       `json:"gid" db:"gid"`
	Name        string    `json:"name" db:"name"`
	Description string    `json:"description" db:"description"`
	CreatedBy   string    `json:"created_by" db:"created_by"`
	UpdatedBy   string    `json:"updated_by" db:"updated_by"`
	DeletedBy   string    `json:"deleted_by" db:"deleted_by"`
	Deleted     bool      `json:"deleted" db:"deleted"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (a AdmGrp) String() string {
	ja, _ := json.Marshal(a)
	return string(ja)
}

// AdmGrps is not required by pop and may be deleted
type AdmGrps []AdmGrp

// String is not required by pop and may be deleted
func (a AdmGrps) String() string {
	ja, _ := json.Marshal(a)
	return string(ja)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (a *AdmGrp) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: a.ID, Name: "ID"},
		&validators.IntIsPresent{Field: a.Gid, Name: "Gid"},
		&validators.StringIsPresent{Field: a.Name, Name: "Name"},
		&validators.StringIsPresent{Field: a.Description, Name: "Description"},
		&validators.StringIsPresent{Field: a.CreatedBy, Name: "CreatedBy"},
		&validators.StringIsPresent{Field: a.UpdatedBy, Name: "UpdatedBy"},
		&validators.StringIsPresent{Field: a.DeletedBy, Name: "DeletedBy"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (a *AdmGrp) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (a *AdmGrp) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
