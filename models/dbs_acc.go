package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop/v6"
	"github.com/gobuffalo/validate/v3"
	"github.com/gobuffalo/validate/v3/validators"
)

// DbsAcc is used by pop to map your dbs_accs database table to your go code.
type DbsAcc struct {
	ID          int       `json:"id" db:"id"`
	Uid         int       `json:"uid" db:"uid"`
	Name        string    `json:"name" db:"name"`
	Description string    `json:"description" db:"description"`
	CreatedBy   string    `json:"created_by" db:"created_by"`
	UpdatedBy   string    `json:"updated_by" db:"updated_by"`
	DeletedBy   string    `json:"deleted_by" db:"deleted_by"`
	Deleted     bool      `json:"deleted" db:"deleted"`
	CreatedAt   time.Time `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (d DbsAcc) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// DbsAccs is not required by pop and may be deleted
type DbsAccs []DbsAcc

// String is not required by pop and may be deleted
func (d DbsAccs) String() string {
	jd, _ := json.Marshal(d)
	return string(jd)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (d *DbsAcc) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: d.ID, Name: "ID"},
		&validators.IntIsPresent{Field: d.Uid, Name: "Uid"},
		&validators.StringIsPresent{Field: d.Name, Name: "Name"},
		&validators.StringIsPresent{Field: d.Description, Name: "Description"},
		&validators.StringIsPresent{Field: d.CreatedBy, Name: "CreatedBy"},
		&validators.StringIsPresent{Field: d.UpdatedBy, Name: "UpdatedBy"},
		&validators.StringIsPresent{Field: d.DeletedBy, Name: "DeletedBy"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (d *DbsAcc) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (d *DbsAcc) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
